# frozen_string_literal: true

require './spec/spec_helper'

RSpec.describe TanukiEmoji::Index do
  subject { described_class.instance }

  let(:horse_emoji) do
    TanukiEmoji::Character.new('horse',
                               codepoints: "\u{1f434}",
                               alpha_code: ':horse:',
                               description: 'horse face',
                               category: 'nature')
  end

  context '#add' do
    before do
      subject.reset!(reload: false)
    end

    context 'when emoji has not been indexed before' do
      it 'adds a new item to the index' do
        expect { subject.add(horse_emoji) }.to change { subject.all.size }.by(1)
      end
    end

    context 'with already indexed emoji with same codepoint' do
      it 'raises an error' do
        subject.add(horse_emoji)

        another_horse = TanukiEmoji::Character.new('another horse',
                                                   codepoints: horse_emoji.codepoints,
                                                   alpha_code: ':another_horse',
                                                   description: 'another horse',
                                                   category: 'nature')

        expect { subject.add(another_horse) }.to raise_error(TanukiEmoji::CodepointAlreadyIndexedError)
      end
    end

    context 'with already indexed emoji with same alpha_code' do
      it 'raises an error' do
        subject.add(horse_emoji)

        similar_horse = TanukiEmoji::Character.new('similar horse',
                                                   codepoints: horse_emoji.codepoints,
                                                   alpha_code: 'horse',
                                                   description: 'a very similar horse',
                                                   category: 'nature')

        expect { subject.add(similar_horse) }.to raise_error(TanukiEmoji::AlphaCodeAlreadyIndexedError)
      end
    end
  end

  context '#find_by_alpha_code' do
    before do
      subject.reset!
    end

    it 'returns nil when nil value is provided' do
      expect(subject.find_by_alpha_code(nil)).to eq(nil)
    end

    it 'returns an emoji character when an indexed alpha code is provided' do
      expect(subject.find_by_alpha_code('horse')).to eq(horse_emoji)
    end
  end

  context '#find_by_codepoints' do
    before do
      subject.reset!
    end

    it 'returns nil when nil value is provided' do
      expect(subject.find_by_codepoints(nil)).to eq(nil)
    end

    it 'returns an emoji character when an indexed alpha code is provided' do
      expect(subject.find_by_codepoints("\u{1f434}")).to eq(horse_emoji)
    end
  end

  context '#alpha_code_pattern' do
    it 'matches an aliased alpha code' do
      expect(subject.alpha_code_pattern.match?(':+1:')).to be_truthy
    end

    it 'matches a regular indexed alpha code' do
      expect(subject.alpha_code_pattern.match?(':thumbsup:')).to be_truthy
    end
  end

  context '#codepoints_pattern' do
    it 'matches an indexed emoji codepoints' do
      expect(subject.codepoints_pattern.match?('🐴')).to be_truthy
    end

    it 'matches an indexed emoji with requested text presentation' do
      expect(subject.codepoints_pattern.match?("🐴#{TanukiEmoji::Character::PLAIN_VARIATION_SELECTOR_STRING}")).to be_truthy
    end

    context 'with exclude_text_presentation = true' do
      it 'does not match an emoji with requested text presentation' do
        regex = subject.codepoints_pattern(exclude_text_presentation: true)

        expect(regex.match?("🐴#{TanukiEmoji::Character::PLAIN_VARIATION_SELECTOR_STRING}")).to be_falsy
      end
    end

    # `gsub`, as used by GitLab's Banzai::Filter::EmojiFilter#emoji_unicode_element_unicode_filter.
    it 'matches entire thumbsup_tone4 unicode' do
      expect('👍🏾'.gsub(subject.codepoints_pattern, '.')).to eq('.')
    end

    it 'matches entire family_mwgb unicode' do
      expect('👨‍👩‍👧‍👦'.gsub(subject.codepoints_pattern, '.')).to eq('.')
    end

    it 'matches entire woman_facepalming unicode' do
      pending("updated emoji set: https://gitlab.com/gitlab-org/tanuki_emoji/-/merge_requests/30")
      expect('🤦‍♀️'.gsub(subject.codepoints_pattern, '.')).to eq('.')
    end
  end
end
