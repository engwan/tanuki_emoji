# frozen_string_literal: true

require 'rspec'

RSpec.describe TanukiEmoji::Db::UnicodeVersion do
  subject { described_class.new(index: TanukiEmoji.index) }
  before do
    TanukiEmoji.index.reset!(reload: false)

    TanukiEmoji::Db::Gemojione.new(index: TanukiEmoji.index).load!
  end

  describe '#load!' do
    it 'populates indexed emojis with unicode data' do
      expect(TanukiEmoji.index.all.all? { _1.unicode_version.nil? }).to be_truthy

      subject.load!

      expect(TanukiEmoji.index.all.none? { _1.unicode_version.nil? }).to be_truthy
    end
  end
end
