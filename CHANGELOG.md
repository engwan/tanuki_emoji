# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.6.0 (2022-02-16)

### Added

- `Character` responds to `ascii_aliases` which contain ASCII aliases from Gemojione

### Fixed

- Fixed `TanukiEmoji.add` command, which should now require `category:` to be provided
- Fixed issue where `TanukiEmoji::Index#codepoints_pattern` would split apart emoji-modifier pairs

## 0.5.0 - (2021-09-16)

### Added

- Add Category information into `Character`
- Add Character.unicode_version and index in which unicode version an emoji was introduced

## 0.4.0 - (2021-09-07)

### Added

- Index can return the `alpha_code_pattern` and `codepoints_pattern` to be used as text extraction sources

### Fixed

- Fixed `registered sign`, `copyright sign` and `trade mark sign` codepoints from gemojione index

## 0.3.0 - (2021-08-26)

### Changed

- Characters can be compared and will be considered equal when all of its attributes matches
- `:+1:` and `:-1:` which are aliases for `:thumbsup:` and `:thumbsdown:` can now be used with `find_by_alpha_code`
- added tests for both `find_by_alpha_code` and `find_by_codepoints` and make sure `find_by_alpha_code` can handle `nil` correctly

## 0.2.2 - (2021-08-23)

### Changed

- Fixed autoload load_path

## 0.2.1 - (2021-07-09)

### Changed

- Noto Emoji assets were not included due to bug in gemspec code. Now it is.

## 0.2.0 - (2021-07-09)

### Added

- Bundled Noto Emoji assets for each corresponding indexed Emoji
- `Character` responds to `#image_name` pointing to Noto Emoji filenames
- `TanukiEmoji` responds to `.images_path` pointing to Emoji assets folder

## 0.1.0 - (2021-07-04)

### Added

- Initial release with index and Character information support
